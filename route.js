var currentUserHandle = {
    ready: function () {
        return 'undefined' !== typeof Meteor.user();
    }
};

// Global router config
Router.configure({
    loadingTemplate: 'loading',
    layoutTemplate: 'layout',
    waitOn: function() {
        return [
            Meteor.subscribe('projects'),
            Meteor.subscribe('persons'),
            Meteor.subscribe('meetups'),
            currentUserHandle
        ];
    },
});

var OnBeforeActions;

OnBeforeActions = {
    loginRequired: function(pause) {
        if (!Meteor.userId()) {
            Router.go('landing');
        }
    }
};

Router.onBeforeAction('loading');

Router.onBeforeAction(OnBeforeActions.loginRequired, {
    only: ['profile-submits', 'person-details', 'personProfile', 'createProjects']
});

Router.map(function() {
    this.route('details', {
        path: '/details/:_id',
        data: function() {
            return Projects.findOne(this.params._id);
        },
        onAfterAction: function() {
            var project = this.data();
            if (project) {
                SEO.set({
                    title: project.title,
                    meta: {
                        'description': project.description
                    },
                    og: {
                        'title': project.title,
                        'description' : project.description,
                        'url' : window.location,
                        'image' : project.photo
                    }
                });
            }
        }
    });
    this.route('landing', {
        path: '/',
    });
    this.route('create');
    this.route('profile-submits', {
        data : function() {
                var sup_projects_ids = Meteor.user().profile.projects || [];
                var supported_projects = Projects.find(
                    { 'published': true, '_id' : {
                        "$in" : sup_projects_ids }}, { sort : {order : 1}});
                return { 'supported_projects' : supported_projects};
            }
    });
    this.route('projectForm', {
        path: 'projectForm/:_id?',
        data : function() {
            if (this.params._id) {
                return { 'project' : Projects.findOne(this.params._id) };
            }
        }
    });
    this.route('allProjects', {
        data : function() {
            return { 'projects' : Projects.find({'published' : true}, { sort : {order : 1} } )};
        }
    });
    this.route('matching', {
        data: function() {
            var current = Persons.findOne({'user' : Meteor.userId()});
            return { 'currentPerson' : current };
        }
    });
    this.route('person-details', {
        data: function() {
            return { 'currentPerson' : Persons.findOne({ 'user' : Meteor.userId() })};
        }
    });
    this.route('personProfile', {
        path: 'personProfile/:_id',
        data : function() {
            return Persons.findOne(this.params._id);
        }
    });
    this.route('messages', {
        data : function() {
            return Persons.findOne({'user' : Meteor.userId()});
        }
    });
    this.route('contacts', {
        data : function() {
            return Persons.findOne({'user' : Meteor.userId()});
        }
    });
    this.route('createProjects', {
        data : function() {
            var created_projects = Projects.find({ 'user' : Meteor.userId()});
            return  { 'created_projects' : created_projects};
        }
    });
    this.route('clubs', {
        data : function() {
            var today = new Date();
            today.setHours(0,0,0,0);
            return { 'meetups' : Meetups.find({date : { $gte : today}}, { sort : {date : 1} })};
        }
    });

    this.route('setupApp');
});
