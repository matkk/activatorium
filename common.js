Schemas = {};

Schemas.project = new SimpleSchema({
    user: {
        type: String,
        label: "Author",
        optional: true,
    },
    title: {
        type: String,
        label: "Tytuł",
        max: 200
    },
    description: {
        type: String,
        label: "Krótki (jednozdaniowy) opis projektu",
    },
    photo: {
        type: String,
        label: "Link do zdjęcia"
    },
    video: {
        type: String,
        label: "Link do wideo na YouTube (wskazany ale nie wymagany :)",
        optional : true
    },
    place: {
        type: String,
        label: "Miejsce"
    },
    tags: {
        type: [String],
        label: "Tagi",
        optional: true,
        autoform: {
            omit: true
        }
    },
    published: {
        type: Boolean,
        label : "Dostepne na stronie",
        autoform: {
            omit: true
        },
        defaultValue: false
    },
    long_description: {
        type : String,
        label: "Przebieg oraz detale dotyczące spotkania (można używać html)",
        max: 10000,
        optional: true,
        autoform: {
            omit: true,
        }
    },

    what_can_give : {
        type : String,
        label : "Co możemy dać"
    },

    who_looking_for : {
        type : String,
        label : "Kogo szukamy"
    }
});

Message = new SimpleSchema({
    from: {
        type: String
    },
    to: {
        type: String
    },
    message: {
        type: String
    },
    time_sent: {
        type: Date
    }
});

Schemas.person = new SimpleSchema({
    user: {
        type: String,
        label: "User",
        optional: true,
        autoform : {
            omit: true
        }
    },
    name: {
        type: String,
        label: "Imię i nazwisko"
    },
    what_can_give : {
        type: String,
        label: "Co mogę dać",
    },
    what_looking_for : {
        type: String,
        label: "Czego/Kogo szukam",
        optional: true
    },
    person_type: {
        type: String,
        label: "Specjalność",
        allowedValues: ['technical', 'ux', 'biznes'],
        optional: true,
        autoform: {
            options: [
                {label: "Programista", value: "technical"},
                {label: "Grafik/UX", value: "ux"},
                {label: "Biznes", value: "biznes"}
            ]
        }
    },
    project_desc: {
        type: String,
        label: "Krótki opis mojego projektu",
        optional: true,
        autoform: {
            omit: true
        }
    },
    searchable : {
        type: Boolean,
        label: "Czy inny mogą znaleźć twój profil",
        defaultValue: true
    },
    messages : {
        type: [Message],
        label: "Twoje wiadomości",
        defaultValue: [],
        autoform: {
            omit: true,
        }
    },
});

Schemas.meetup = new SimpleSchema({
    title : {
        type: String,
        label: "Nazwa spotkania"
    },
    link : {
        type : String,
        label: "Link do spotkania na fb"
    },
    date : {
        type : Date,
        label: "Data spotkania"
    }
});
