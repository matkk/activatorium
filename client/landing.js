Template.landing.helpers({
    showSecondPoint : function() {
        return Session.get("showsecond") === true;
    },
    showThirdPoint : function() {
        return Session.get("showthird") === true;
    }
});


Template.landing.rendered = function() {
    window.onscroll = function (event) {
        if (!$('.propositions:first').offset()) return;
        if ($(window).scrollTop() + $(window).height() >
            $('.propositions:first').offset().top + $('.propositions:first').height() + 100) {
            Session.set("showsecond", true);
        }

        if ($(window).scrollTop() + $(window).height() >
            $('.propositions:first').offset().top + $('.propositions:first').height() * 2 + 200) {
            Session.set("showthird", true);
            $(window).unbind('scroll');
        }
    };

};
