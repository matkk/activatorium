
Template.personProfile.helpers({
    personNameFromId : function(personId) {
        var person = Persons.findOne(personId);
        return person.name;
    },
    messageSent : function() {
        if (Session.get('message-sent') == this._id) {
            return true;
        }
        else {
            return false;
        }
    },
    communicationWithMe : function() {
        my_person = Persons.findOne({'user': Meteor.userId()});
        var messages = this.communication_with(my_person._id);
        for (var i=0; i < messages.length; i+=1) {
            if (messages[i].from == my_person._id) {
                messages[i].my = 'my';
            } else {
                messages[i].my = false;
            }
        }
        return messages;
    }
});

Template.personProfile.events({
    'click .send-person-message' : function(evt) {
        var id = this._id;
        var message = $('textarea[name="person-message"]').val();
        Meteor.call('sendMessage', this._id, message, function(err, data) {
            Session.set('message-sent', id);
            $('textarea[name="person-message"]').val("");
        });
    }

});
