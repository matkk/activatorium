
AutoForm.hooks({
    insertPersonForm: {
        before: {
            insert: function(doc, template) {
                doc.user = Meteor.userId();
                doc.name = Meteor.user().profile.name;
                return doc;
            },
        },
        onSuccess: function(operation, result, template) {
            Router.go('matching');
        },
    },
    updatePersonForm: {
        before: {
            update: function(docId, modifier, template) {
                modifier.$set.name = Meteor.user().profile.name;
                return modifier;
            }
        },
        onSuccess: function(operation, result, template) {
            Router.go('matching');
        },
    }
});
