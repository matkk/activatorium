
Template.matching.helpers({
    filteredPersons : function() {
        var filter = Session.get('filter-people');
        if (!filter) return Persons.find({'user' : { $ne : Meteor.userId()}});
        else {
            return Persons.find({
                '$or' : [
                    {'project_desc' :  { $regex: filter, $options: 'i'}},
                    {'what_can_give' : { $regex: filter, $options: 'i'}}
                ],
                'user' : { $ne : Meteor.userId()}
            });
        }
    }
});

Template.matching.events({
    'keyup .search-person' : function(evt) {
        var text = evt.target.value;
        Session.set('filter-people', text);
    }
});
