Template.user_loggedout.events({
    "click #login" : function(e, tmpl) {
        Meteor.loginWithFacebook({
        }, function(err) {
            if (err) {
                // error handing
            }
            else {
                // success
            }
        });
    }
});

Template.user_loggedin.events({
    "click #logout" : function(e, tmpl) {
        Meteor.logout(function(err){
            if (err) {
                // error handling
            }
            else {
                // success
            }
        });
    }
});
