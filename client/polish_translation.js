accountsUIBootstrap3.map('pl', {
    _loginButtonsLoggedOutSingleLoginButton: {
        signInWith: 'Zaloguj się',
        configure: 'Konfiguruj',
        login: 'Logowanie'
    },
    _loginButtonsLoggedInSingleLogoutButton: {
        signOut: 'Wyloguj się'
    },
});

accountsUIBootstrap3.setLanguage('pl');
