Template._loginButtonsLoggedInDropdown.events({
    'click #login-buttons-your-submits': function(event) {
        event.stopPropagation();
        Template._loginButtons.toggleDropdown();
        Router.go('profile-submits');
    }
});
