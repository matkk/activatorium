Template.details.helpers({
    showEmailInput : function() {
        return true;
    },

    userJoined : function() {
        return _.indexOf(Meteor.user().profile.projects, this._id) !== -1;
    },

    errors : function() {
        return Session.get(this._id + ".errors");
    },

    canPublish : function () {
        return Meteor.user.profile.name == "Mateusz Klimek";
    },

    createdByMe : function() {
        console.log(Meteor.user()._id);
        return  Meteor.user()._id == this.user;
    }
});

Template.details.events({
    'submit form': function (evt, template) {
        evt.preventDefault();
        var email = template.find("#email-input").value;
        if (email && validateEmail(email)) {
            Meteor.call('join', this._id, email);
            Session.set(this._id + ".errors", null);
        } else {
            Session.set(this._id + ".errors", "fodaj poprawny adres email.");
        }
    },
    'click .facebook-share-button': function (evt, template) {
        FB.ui({
            method: 'share',
            href: window.location.href,
        }, function(response){});
    },
    'click .publish-button' : function(evt, template) {
        Meteor.call('publish', this._id);
    }

});
