Template.projectBox.helpers({
    canEdit : function() {
        return Meteor.userId() && this.user == Meteor.userId();
    }
});
