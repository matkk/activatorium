
AutoForm.hooks({
    insertProjectForm: {
        before: {
            insert: function(doc, template) {
                doc.long_description = $('#summernote-project-form').code();
                doc.user = Meteor.userId();
                return doc;
            },
        },
        onSuccess: function(operation, result, template) {
            Router.go('createProjects');
        },
    },
    updateProjectForm: {
        before: {
            update: function(docId, modifier, template) {
                modifier.$set.long_description = $('#summernote-project-form').code();
                return modifier;
            }
        },
        onSuccess: function(operation, result, template) {
            Router.go('details', { '_id' : template.data.doc._id});
        },
    }
});

Template.creatorFormBase.rendered = function() {
    var project = this.data.project;
    var myInterval = setInterval(function() {
        try {
            $('#summernote-project-form').summernote({
                height: 200,
                oninit: function() {
                    if (project)  {
                        $('#summernote-project-form').code(project.long_description);
                    }
                }
            });
            clearInterval(myInterval);
        } catch(all) {}
    }, 100);
};

