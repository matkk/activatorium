if(Meteor.isClient) {
    window.fbAsyncInit = function() {
        FB.init({
            appId      : Meteor.settings.public.fb_app,
            status     : true,
            xfbml      : true,
            version    : 'v2.1',
        });
    };
}
