Person = function (doc) {
    _.extend(this, doc);
};

_.extend(Person.prototype, {
    email : function() {
        // Not implemented for now
        // return Meteor.users.findOne(this.user).services.facebook.email;
        return;
    },

    person_type_desc : function() {
        var s = this.person_type;
        if (s == "technical") {
            return "Programista";
        }
        if (s == "ux") {
            return "Grafik/UX";
        }
        if (s == "biznes") {
            return "Biznes";
        }
    },

    my_contacts : function() {
        var contacts = Persons.find({
            '_id' : { $in : _.union(_.pluck(this.messages, 'from'), _.pluck(this.messages, 'to'))},
            'user' : { $ne : this.user}
        });
        return contacts;
    },

    communication_with : function(personId) {
        var messages = _.filter(this.messages, function(msg){
            if (msg.from == personId || msg.to == personId) return true;
            else return false;
        });
        return messages.reverse();
    },

    projects : function() {
        var user = this.user;
        return Projects.find({'user' : user});
    },

    num_messages : function() {
        return this.messages.length;
    },

    facebook_profile : function() {
        //Not now, think about security
        //return Meteor.users.findOne(this.user).services.facebook.link;
    }
});

Persons = new Mongo.Collection('persons', {
    transform: function (doc) { return new Person(doc); }
});

Persons.attachSchema(Schemas.person);

Meteor.methods({
    sendMessage : function(personId, message) {
        user = Meteor.user();
        sending_person = Persons.findOne({'user' : Meteor.userId()});
        receiving_person = Persons.findOne(personId);

        var messageObj = {
                    'from' : sending_person._id,
                    'to': personId,
                    'message': message,
                    'time_sent' : new Date()
        };

        Persons.update(sending_person._id, { $push : { 'messages' : messageObj}});
        Persons.update(personId,  { $push : { 'messages' : messageObj}});

        if (Meteor.isServer) {
            receiving_user = Meteor.users.findOne(receiving_person.user);
            Email.send({
                from: user.profile.name+" via Activatorium <info@activatorium.pl>",
                to: receiving_user.services.facebook.email,
                replyTo: user.services.facebook.email,
                subject: "Dostałeś wiadomość",
                text: message
            });
        }
    }
});
