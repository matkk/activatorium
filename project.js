Project = function (doc) {
    _.extend(this, doc);
};

_.extend(Project.prototype, {
    daysLeft : function () {
        now = moment(new Date());
        to = moment(this.end_date);
        return to.diff(now, "days");
    },
    percentGathered : function () {
        var percent =  this.emails.length * 100 / this.goal;
        return percent;
    },

    inner_video_url : function() {
        return this.video.replace("watch?v=", "v/");
    },

    personId : function() {
        var person = Persons.findOne({'user' : this.user});
        if (person) return person._id;
        else return null;
    }

});

Projects = new Mongo.Collection('projects', {
    transform: function (doc) { return new Project(doc); }
});

Projects.attachSchema(Schemas.project);

Meteor.methods({
    join : function(projectId, email) {
        user = Meteor.user();
        Projects.update(projectId, {
            $push : {
                users :  { "userId": Meteor.userId()}
            }
        });

        Meteor.users.update(this.userId, {
            $addToSet : {
                "profile.projects" : projectId
            }
        });

        if (Meteor.isServer) {
            Email.send({
                from: 'mail@activatorium.pl',
                to: 'mat@activatorium.pl',
                replyTo: '',
                subject: "[STARTER] subscription",
                text: "User with email: " + email + " subcbired to project: " + projectId
            });
        }
    },
    publish : function(projectId) {
        user = Meteor.user();
        if (user.profile.name !== "Mateusz Klimek") {
            return;
        }
        Projects.update(projectId, {
            $set : { "published" : true}
        });
    }
});

Meteor.users.deny({update: function () { return true; }});

