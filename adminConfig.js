AdminConfig = {
    adminEmails: ['matkk11@gmail.com', 'elzbieta.minda@vp.pl'],
    collections: {
        Projects: {
            tableColumns: [
                {label: 'Title', name: 'title'},
                {label: 'User', name: 'user', collection: 'Users'}
            ]
        },
        Meetups: {},
        Persons: {
            icon : 'users',
            tableColumns : [
                {label : 'Name', name: 'name'}
            ]
        }
    }
};
