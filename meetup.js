Meetup = function (doc) {
    _.extend(this, doc);
};

_.extend(Meetup.prototype, {
});

Meetups = new Mongo.Collection('meetups', {
    transform: function (doc) { return new Meetup(doc); }
});

Meetups.attachSchema(Schemas.meetup);
