function getFbPicture(id) {
    return "http://graph.facebook.com/" + id + "/picture/?type=large";
}

Accounts.onCreateUser(function(options, user) {
    var email = user.services.facebook.email;
    user.emails = [{"address" : email, "verified" : true}];
    user.profile = {
        'avatar_url' : getFbPicture(user.services.facebook.id),
        'name' : user.services.facebook.name,
        'first_name' : user.services.facebook.first_name
    };
    return user;
});
