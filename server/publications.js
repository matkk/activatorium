Meteor.publish('projects', function() {
  return Projects.find();
});

Meteor.publish('persons', function() {
    return Persons.find({"$or" : [{"searchable" : true}, {"user" : this.userId}]});
});

Meteor.publish('meetups', function() {
    return Meetups.find();
});
